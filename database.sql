create database UBERHACK;
use uberhack;

insert into RELATO (nm_local, nr_latitude, nr_longitude, nr_pontos) values (
	'prefeitura de porto alegre',
	-30.027990,
	-51.228052,
	2,
)

insert into RELATO (bl_voce_sabia, ds_relato, id_local) values (
	true,
	'o edifício foi tombado pelo município em 21 de novembro de 1979 e passou por uma reforma total em 2003, adaptando diversos espaços internos para exposições de arte e para guarda do Acervo Artístico da Prefeitura de Porto Alegre.',
	1,
)

insert into RELATO (bl_voce_sabia, ds_relato, id_local) values (
	false,
	'Muitos momentos aqui, vim desde cedo com os meus avós. Reviver isso foi mágico :)',
	1,
)

insert into RELATO (bl_voce_sabia, ds_relato, id_local) values (
	false,
	'Onde aprendi a gostar da Cidade, apesar de tudo. Tire 10 minutos olhando essa praça e ajude a ocupá-la. É um dos lugares mais bonitos de POA e é de todos nós…',
	1,
)