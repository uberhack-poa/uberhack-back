package br.com.uberhack.service.local;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.uberhack.model.Local;
import br.com.uberhack.repository.LocalRepository;
import br.com.uberhack.service.AbstractService;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Service
public class LocalService extends AbstractService {

	@Autowired
	private LocalRepository repository;

	public Local loadActive() {
		return repository.findByBlDescobertaDaSemana(true);
	}

}
