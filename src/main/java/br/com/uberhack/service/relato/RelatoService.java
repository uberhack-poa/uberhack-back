package br.com.uberhack.service.relato;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.uberhack.model.Local;
import br.com.uberhack.model.Relato;
import br.com.uberhack.repository.RelatoRepository;
import br.com.uberhack.service.AbstractService;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Service
public class RelatoService extends AbstractService {

	@Autowired
	private RelatoRepository repository;

	public Relato findVoceSabiaByLocal(Long idLocal) {
		Local local = Local.builder().idLocal(idLocal).build();
		return repository.findByBlVoceSabiaAndLocal(true, local).get(0);
	}

	public List<Relato> findByLocal(Long idLocal) {
		Local local = Local.builder().idLocal(idLocal).build();
		return repository.findByBlVoceSabiaAndLocal(false, local);
	}

	public Relato save(Relato relato) {
		return repository.save(relato);
	}
}
