package br.com.uberhack.service.usuario;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.uberhack.dto.usuario.UsuarioBuscaDto;
import br.com.uberhack.dto.usuario.UsuarioCadastroDto;
import br.com.uberhack.mapper.usuario.UsuarioMapper;
import br.com.uberhack.model.Usuario;
import br.com.uberhack.repository.UsuarioRepository;
import br.com.uberhack.service.AbstractService;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Service
public class UsuarioService extends AbstractService {

	@Autowired
	private UsuarioRepository repository;

	public Usuario save(UsuarioCadastroDto usuarioCadastroDto) {
		Usuario model = UsuarioMapper.fromCadastroDtoToModel(usuarioCadastroDto);
		model.encryptPassword();
		return repository.save(model);
	}

	public Usuario load(Long idUsuario) {
		return this.repository.findOne(idUsuario);
	}

	public Usuario findByDsLogin(String dsLogin) {
		return repository.findByDsLogin(dsLogin).orElse(null);
	}

	public List<Usuario> findByName(String nmUsuario) {
		return this.repository.findByNmUsuario(nmUsuario);
	}

	public List<Usuario> findAll() {
		return this.repository.findAll();
	}

	public long count() {
		return this.repository.count();
	}

	public void delete(Long idUsuario) {
		this.repository.delete(idUsuario);
	}

	public void updatePontos(Long nrPontos) {
		Usuario usuario = getPrincipal();
		usuario.setNrPontos(nrPontos);
		this.repository.save(usuario);
	}

}
