package br.com.uberhack.utils;

import static br.com.uberhack.constants.Constants.FORBIDDEN_ACTION;
import static br.com.uberhack.constants.Constants.INVALID_DATA;

import java.util.Objects;

import br.com.uberhack.exception.UberHackException;
import br.com.uberhack.model.Usuario;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

public class SecurityUtils {
	public static void verifyPrincipal(Usuario principal, Usuario owner) {
		if (Objects.isNull(principal) || Objects.isNull(owner)) {
			throw new RuntimeException(INVALID_DATA);
		}

		if (!principal.getIdUsuario().equals(owner.getIdUsuario())) {
			throw new UberHackException(FORBIDDEN_ACTION);
		}
	}
}
