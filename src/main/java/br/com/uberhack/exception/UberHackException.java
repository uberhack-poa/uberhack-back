package br.com.uberhack.exception;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

public class UberHackException extends RuntimeException {
	public UberHackException(String message) {
		super(message);
	}
}
