package br.com.uberhack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.uberhack.model.Local;
import br.com.uberhack.service.local.LocalService;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@RestController
@RequestMapping("/local")
public class LocalController {

	@Autowired
	private LocalService localService;

	@GetMapping
	public Local getDesafioSemanal() {
		return localService.loadActive();
	}

}
