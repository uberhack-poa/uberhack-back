package br.com.uberhack.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.uberhack.model.Relato;
import br.com.uberhack.service.relato.RelatoService;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@RestController
@RequestMapping("/relato")
public class RelatoController {

	@Autowired
	private RelatoService relatoService;

	@GetMapping("/voce-sabia/{idLocal}")
	public Relato findVoceSabiaByLocal(@PathVariable("idLocal") Long idLocal) {
		return relatoService.findVoceSabiaByLocal(idLocal);
	}

	@GetMapping("/{idLocal}")
	public List<Relato> findByLocal(@PathVariable("idLocal") Long idLocal) {
		return relatoService.findByLocal(idLocal);
	}

	@PostMapping
	public Relato save(@RequestBody Relato relato) {
		return relatoService.save(relato);
	}

}
