package br.com.uberhack.dto.login;

import lombok.Data;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Data
public class LoginRequestDto {
	private final String username;
	private final String password;
}
