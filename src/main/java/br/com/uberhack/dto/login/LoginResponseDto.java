package br.com.uberhack.dto.login;

import lombok.Builder;
import lombok.Data;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Data
@Builder
public class LoginResponseDto {
	private final String accessToken;
	private final Long idUsuario;
}
