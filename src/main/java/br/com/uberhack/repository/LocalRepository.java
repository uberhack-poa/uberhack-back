package br.com.uberhack.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.uberhack.model.Local;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

public interface LocalRepository extends CrudRepository<Local, Long> {

	Local findByBlDescobertaDaSemana(Boolean blDescobertaDaSemana);

}
