package br.com.uberhack.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import br.com.uberhack.model.Local;
import br.com.uberhack.model.Relato;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

public interface RelatoRepository extends CrudRepository<Relato, Long> {

	List<Relato> findByBlVoceSabiaAndLocal(Boolean blVoceSabia, Local local);

}
