package br.com.uberhack.mapper.usuario;

import br.com.uberhack.dto.usuario.UsuarioBuscaDto;
import br.com.uberhack.dto.usuario.UsuarioCadastroDto;
import br.com.uberhack.model.Usuario;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

public class UsuarioMapper {
	private UsuarioMapper() {
	}

	public static Usuario fromCadastroDtoToModel(UsuarioCadastroDto cadastroDto) {
		return Usuario.builder()
				.dsEmail(cadastroDto.getEmail())
				.dsLogin(cadastroDto.getEmail())
				.dsSobrenome(cadastroDto.getLastName())
				.nmUsuario(cadastroDto.getFirstName())
				.dsFoto(cadastroDto.getPhoto())
				.idGoogle(cadastroDto.getGoogleId())
				.dsSenha(cadastroDto.generatePassword())
				.build();
	}

}
