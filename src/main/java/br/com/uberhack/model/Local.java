package br.com.uberhack.model;


import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(
		uniqueConstraints = {
				@UniqueConstraint(name = "UK_ID_LOCAL", columnNames = "ID_LOCAL"),
		},
		indexes = {
				@Index(name = "IX_PK_LOCAL", columnList = "ID_LOCAL"),
		}
)
public class Local {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_LOCAL", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idLocal;

	@NotNull
	@Column(nullable = false)
	private BigDecimal nrLatitude;

	@NotNull
	@Column(nullable = false)
	private BigDecimal nrLongitude;

	@NotNull
	@Column(nullable = false)
	private String nmLocal;

	@NotNull
	@Column(nullable = false)
	private Long nrPontos;

	@NotNull
	@Column(nullable = false)
	private Boolean blDescobertaDaSemana;

}
