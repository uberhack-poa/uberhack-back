package br.com.uberhack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(
		uniqueConstraints = {
				@UniqueConstraint(name = "UK_ID_CONQUISTA", columnNames = "ID_CONQUISTA"),
		},
		indexes = {
				@Index(name = "IX_PK_CONQUISTA", columnList = "ID_CONQUISTA"),
		}
)
public class Conquista {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_CONQUISTA", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idConquista;

	@ManyToOne
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", foreignKey = @ForeignKey(name = "FK_CONQUISTA_USUARIO"))
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "ID_LOCAL", referencedColumnName = "ID_LOCAL", foreignKey = @ForeignKey(name = "FK_CONQUISTA_LOCAL"))
	private Local local;

	@NotNull
	@Column(nullable = false)
	private Long nrPontosNoMomento;

}
