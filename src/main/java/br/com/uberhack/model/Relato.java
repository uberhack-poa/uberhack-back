package br.com.uberhack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author alexia.dorneles
 * @email alexiadorneles02@gmail.com
 */

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(
		uniqueConstraints = {
				@UniqueConstraint(name = "UK_ID_RELATO", columnNames = "ID_RELATO"),
		},
		indexes = {
				@Index(name = "IX_PK_RELATO", columnList = "ID_RELATO"),
		}
)
public class Relato {

	@Id
	@Column(name = "ID_RELATO", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idRelato;

	@NotNull
	@Column(nullable = false)
	private String dsRelato;

	@JoinColumn(name = "ID_LOCAL", referencedColumnName = "ID_LOCAL", foreignKey = @ForeignKey(name = "FK_RELATO_LOCAL"))
	@ManyToOne
	private Local local;

	@NotNull
	@Column(nullable = false)
	private Boolean blVoceSabia;

}
